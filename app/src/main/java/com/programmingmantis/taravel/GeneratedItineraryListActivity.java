package com.programmingmantis.taravel;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.programmingmantis.taravel.Adapter.GeneratedItineraryAdapter;
import com.programmingmantis.taravel.Objects.TravelObject;
import com.programmingmantis.taravel.Util.DatabaseUtil;

import java.util.ArrayList;
import java.util.List;

public class GeneratedItineraryListActivity extends AppCompatActivity {

    private ListView listView;

    private List<TravelObject> travelObjects = new ArrayList<>();
    GeneratedItineraryAdapter itineraryAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generated_itinerary_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //toolbar.setTitle("Generated Itinerary List");

        getSupportActionBar().setTitle("Generated Itinerary List");

        listView = (ListView) findViewById(R.id.listView);

        travelObjects.addAll((List<TravelObject>) getIntent().getSerializableExtra("travel_object_list"));

        itineraryAdapter = new GeneratedItineraryAdapter(this, R.layout.travel_object, travelObjects);

        listView.setAdapter(itineraryAdapter);
        //setListViewHeightBasedOnChildren(listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                final TravelObject currentTravelObject = (TravelObject) listView.getItemAtPosition(i);

                AlertDialog.Builder builder = new AlertDialog.Builder(GeneratedItineraryListActivity.this);

                builder.setTitle(currentTravelObject.destination_name);

                String details =
                        "Emotion: " + currentTravelObject.emotion + "\n" +
                                "Purpose: " + currentTravelObject.purpose + "\n\n" +

                                "Destination Description: " + currentTravelObject.destination_desc + "\n" +
                                "Food Description: \t\t" + currentTravelObject.food_desc + "\n\n" +

                                "Pickup Location: " + currentTravelObject.pickup_location + "\n" +
                                "Date start: " + currentTravelObject.date_start + "\n\n" +

                                "Num of pax: " + currentTravelObject.num_of_pax + "\n" +
                                "Package Price: \t\t" + currentTravelObject.package_price + "\n" +
                                "Total Price: \t\t" + currentTravelObject.total_price + "\n";

                builder.setMessage(details);

                builder.setPositiveButton("BOOK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        DatabaseUtil databaseUtil = new DatabaseUtil();
                        databaseUtil.addData(currentTravelObject);
                        finish();
                    }
                });

                builder.show();

            }
        });

        //createTravelObjects();

    }


    /*public List<TravelObject> createTravelObjects(){

        itineraryAdapter.notifyDataSetChanged();

        return travelObjects;

    }*/

    /**** Method for Setting the Height of the ListView dynamically.
     **** Hack to fix the issue of not showing all the items of the ListView
     **** when placed inside a ScrollView  ****/
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AbsListView.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

}
