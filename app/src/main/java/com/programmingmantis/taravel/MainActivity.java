package com.programmingmantis.taravel;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.programmingmantis.taravel.Adapter.TravelListAdapter;
import com.programmingmantis.taravel.Objects.TravelObject;
import com.programmingmantis.taravel.Util.BookingActivity;
import com.programmingmantis.taravel.Util.DatabaseUtil;
import com.programmingmantis.taravel.Util.FireBaseUtil;
import com.programmingmantis.taravel.Util.SharedPrefsUtils;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class MainActivity extends AppCompatActivity {

    private View content;
    private ListView listView;

    TravelListAdapter travelAdapter = null;
    private LinkedHashMap<String, TravelObject> travelObjects = new LinkedHashMap<String, TravelObject>();


    //private Button mBtnSignOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, BookingActivity.class);
                startActivity(intent);
            }
        });

        //mBtnSignOut = (Button) findViewById(R.id.btn_signout);

        content = findViewById(R.id.content_main);
        listView = (ListView) content.findViewById(R.id.listView);
        Log.e("travelObject size", "" + travelObjects.size());

        travelAdapter = new TravelListAdapter(this, R.layout.travel_object, travelObjects);

        listView.setAdapter(travelAdapter);
        setListViewHeightBasedOnChildren(listView);

        createTravelObjects();


        /*mBtnSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new FireBaseUtil().getFirebaseAuth().signOut();
                SharedPrefsUtils.removeAllPreference(MainActivity.this);
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();
            }
        });*/


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.action_logout){

            new FireBaseUtil().getFirebaseAuth().signOut();
            SharedPrefsUtils.removeAllPreference(MainActivity.this);
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();

        }

        return super.onOptionsItemSelected(item);
    }

    public HashMap<String, TravelObject> createTravelObjects(){

        DatabaseUtil databaseUtil = new DatabaseUtil();
        databaseUtil.getItineraryByUser(new DatabaseUtil.DataListenerWithReturn() {
            @Override
            public void onDataAdded(String key, Object object) {
                travelObjects.put(key, (TravelObject) object);
                travelAdapter.addAll(travelObjects);
                setListViewHeightBasedOnChildren(listView);
            }

            @Override
            public void onDataRemoved(String key) {
                travelObjects.remove(key);
                travelAdapter.addAll(travelObjects);
                setListViewHeightBasedOnChildren(listView);
            }
        });

        return travelObjects;

    }

    /**** Method for Setting the Height of the ListView dynamically.
     **** Hack to fix the issue of not showing all the items of the ListView
     **** when placed inside a ScrollView  ****/
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AbsListView.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

}
