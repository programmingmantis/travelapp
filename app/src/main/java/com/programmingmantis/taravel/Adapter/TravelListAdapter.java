package com.programmingmantis.taravel.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.programmingmantis.taravel.Objects.TravelObject;
import com.programmingmantis.taravel.R;
import com.programmingmantis.taravel.Util.DatabaseUtil;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by percivalruiz on 12/13/16.
 */

public class TravelListAdapter extends BaseAdapter {

    Context context;
    int resource;
    private LinkedHashMap<String, TravelObject> objectHashMap;
    private ArrayList<String> mKeys;


    public LinkedHashMap<String, TravelObject> getObjectHashMap() {
        return objectHashMap;
    }

    public void setObjectHashMap(LinkedHashMap<String, TravelObject> objectHashMap) {
        this.objectHashMap = objectHashMap;
    }

    public TravelListAdapter(Context context, int resource, LinkedHashMap<String, TravelObject> objectHashMap) {

        this.context = context;
        this.resource = resource;
        this.objectHashMap = new LinkedHashMap<>();
        this.objectHashMap.putAll(objectHashMap);
        mKeys = new ArrayList<>();
        mKeys.addAll(this.objectHashMap.keySet());

    }

    public void addAll(LinkedHashMap<String, TravelObject> objectHashMap) {
        this.objectHashMap.clear();
        this.objectHashMap.putAll(objectHashMap);
        mKeys.clear();
        mKeys.addAll(this.objectHashMap.keySet());
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(resource, parent, false);

        }

        final TravelObject currentObject = getItem(position);
        String key = mKeys.get(position);

        LinearLayout ll_layout = (LinearLayout) convertView.findViewById(R.id.ll_layout);
        ll_layout.setTag(key);
        ll_layout.setLongClickable(true);
        ll_layout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                final String key = (String) view.getTag();

                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                builder.setTitle("Delete Booking");

                builder.setMessage("Are you sure you want to delete this booking?");

                builder.setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {


                        DatabaseUtil databaseUtil = new DatabaseUtil();
                        databaseUtil.deleteItineraryByKey(key, new DatabaseUtil.DataListenerWithReturn() {

                            @Override
                            public void onDataAdded(String key, Object object) {

                            }

                            @Override
                            public void onDataRemoved(String key) {

                            }
                        });

                    }
                });

                builder.show();

                return true;
            }
        });

        ll_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                builder.setTitle(currentObject.destination_name);

                String details =
                        "Emotion: " + currentObject.emotion + "\n" +
                                "Purpose: " + currentObject.purpose + "\n\n" +

                        "Destination Description: " + currentObject.destination_desc + "\n" +
                                "Food Description: \t\t" + currentObject.food_desc + "\n\n" +

                                "Pickup Location: " + currentObject.pickup_location + "\n" +
                                "Date start: " + currentObject.date_start + "\n\n" +

                                "Num of pax: " + currentObject.num_of_pax + "\n" +
                                "Package Price: \t\t" + currentObject.package_price + "\n" +
                                "Total Price: \t\t" + currentObject.total_price + "\n";

                builder.setMessage(details);

                builder.show();

            }
        });


        TextView tv_place = (TextView) convertView.findViewById(R.id.tv_place);
        tv_place.setText(currentObject.destination_name);

        TextView tv_date = (TextView) convertView.findViewById(R.id.tv_date);
        tv_date.setText(currentObject.date_start);

        return convertView;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getCount() {
        return mKeys.size();
    }

    @Override
    public TravelObject getItem(int i) {
        return objectHashMap.get(mKeys.get(i));
    }
}
