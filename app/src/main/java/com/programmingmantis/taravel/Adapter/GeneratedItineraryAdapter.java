package com.programmingmantis.taravel.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.programmingmantis.taravel.Objects.TravelObject;
import com.programmingmantis.taravel.R;

import java.util.List;

/**
 * Created by GDS LINK ASIA on 12/15/2016.
 */

public class GeneratedItineraryAdapter extends BaseAdapter{

    Context context;
    int resource;
    private List<TravelObject> objectList;

    public GeneratedItineraryAdapter(Context context, int resource, List<TravelObject> objectList) {
        this.context = context;
        this.resource = resource;
        this.objectList = objectList;
    }

    /*public void addAll(List<TravelObject> objectList) {
        this.objectList.clear();
        this.objectList.putAll(objectList);
        mKeys.clear();
        mKeys.addAll(this.objectList.keySet());
        notifyDataSetChanged();
    }*/


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(resource, parent, false);

        }

        final TravelObject currentObject = getItem(position);
        //String key = mKeys.get(position);

        LinearLayout ll_layout = (LinearLayout) convertView.findViewById(R.id.ll_layout);

        TextView tv_place = (TextView) convertView.findViewById(R.id.tv_place);
        tv_place.setText((position+1) + "." + " " + currentObject.destination_name);

        TextView tv_date = (TextView) convertView.findViewById(R.id.tv_date);
        tv_date.setText(currentObject.destination_desc);

        return convertView;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getCount() {
        return objectList.size();
    }

    @Override
    public TravelObject getItem(int i) {
        return objectList.get(i);
    }


}
