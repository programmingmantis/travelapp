package com.programmingmantis.taravel.Objects;

import java.io.Serializable;

/**
 * Created by GDS LINK ASIA on 12/10/2016.
 */

public class TravelObject implements Serializable {


    public String username;
    public String name;
    public String contact;
    public String email;
    public String num_of_pax;
    public String emotion;
    public String purpose;
    public String date_start;
    public String date_end;
    public String time_start;
    public String time_end;
    public String pickup_location;
    public String destination_name;
    public String destination_desc;
    public String accomodation_place;
    public String num_of_days;
    public String destination_price;
    public String transportation_price;
    public String food_price;
    public String accommodation_price;
    public String total_price;
    public String id;
    // newly added
    // price of the package per pax if accommodation, transpo, food are not availed
    public String package_price;

    public String food_desc;
    // end of newly added

    public TravelObject(){

    }

    public TravelObject(String num_of_pax, String emotion, String purpose, String date_start,
                        String pickup_location, String destination_name, String destination_desc,
                        String package_price, String food_desc, String total_price) {
        /*this.username = username;
        this.name = name;
        this.contact = contact;
        this.email = email;*/
        this.num_of_pax = num_of_pax;
        this.emotion = emotion;
        this.purpose = purpose;
        this.date_start = date_start;
        /*this.date_end = date_end;
        this.time_start = time_start;
        this.time_end = time_end;*/
        this.pickup_location = pickup_location;
        this.destination_name = destination_name;
        this.destination_desc = destination_desc;
        /*this.accomodation_place = accomodation_place;
        this.num_of_days = num_of_days;
        this.destination_price = destination_price;
        this.transportation_price = transportation_price;
        this.food_price = food_price;
        this.accommodation_price = accommodation_price;*/
        this.package_price = package_price;
        this.food_desc = food_desc;
        this.total_price = total_price;
    }

    @Override
    public String toString() {
        return id + " " + destination_desc;
    }
}
