package com.programmingmantis.taravel.Util;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.programmingmantis.taravel.Objects.TravelObject;

/**
 * Created by aizer on 12/12/2016.
 */

public class DatabaseUtil {

    private FireBaseUtil fireBaseUtil;
    private String mUserId;
    private DatabaseReference mDatabaseReference;
    private FirebaseDatabase mFirebaseDatabase;

    public DatabaseUtil() {
        this.fireBaseUtil = new FireBaseUtil();
        mUserId = fireBaseUtil.getFirebaseUser().getUid();
    }

    public FireBaseUtil getFireBaseUtil() {
        return fireBaseUtil;
    }

    public void setFireBaseUtil(FireBaseUtil fireBaseUtil) {
        this.fireBaseUtil = fireBaseUtil;
    }

    public void addData(TravelObject travelObject){
        this.fireBaseUtil.getDatabaseReference().child("users").child(mUserId).child("travels").push().setValue(travelObject);
    }

    public void updateDate(TravelObject travelObject, String mDataId) {
        this.fireBaseUtil.getDatabaseReference().child("users").child(mUserId).child("travels").child(mDataId).setValue(travelObject);
    }

    private void refreshItinerary(DataListenerWithReturn dataListenerWithReturn, DataSnapshot dataSnapshot){
        TravelObject post = dataSnapshot.getValue(TravelObject.class);
        post.id = dataSnapshot.getKey();
        dataListenerWithReturn.onDataAdded(dataSnapshot.getKey(), post);
    }

    public void getIteneraryByKey(String key, final DataListenerWithReturn dataListenerWithReturn){
        mFirebaseDatabase = fireBaseUtil.getFirebaseDatabase();
        mDatabaseReference = mFirebaseDatabase.getReference("users");
        mDatabaseReference.child(fireBaseUtil.getFirebaseUser().getUid()).child("travels").child(key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                TravelObject post = dataSnapshot.getValue(TravelObject.class);
                post.id = dataSnapshot.getKey();
                dataListenerWithReturn.onDataAdded(dataSnapshot.getKey(), post);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void deleteItineraryByKey(String key, final DataListenerWithReturn dataListenerWithReturn){
        mFirebaseDatabase = fireBaseUtil.getFirebaseDatabase();
        mDatabaseReference = mFirebaseDatabase.getReference("users");
        mDatabaseReference.child(fireBaseUtil.getFirebaseUser().getUid()).child("travels").child(key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dataSnapshot.getRef().removeValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getItineraryByUser(final DataListenerWithReturn dataListenerWithReturn){
        mFirebaseDatabase = fireBaseUtil.getFirebaseDatabase();
        mDatabaseReference = mFirebaseDatabase.getReference("users");
        mDatabaseReference.child(fireBaseUtil.getFirebaseUser().getUid()).child("travels").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                refreshItinerary(dataListenerWithReturn, dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                refreshItinerary(dataListenerWithReturn, dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                dataListenerWithReturn.onDataRemoved(dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                refreshItinerary(dataListenerWithReturn, dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public interface DataListener{
        public void onDataChanged();
    }

    public interface DataListenerWithReturn{
        public void onDataAdded(String key, Object object);
        public void onDataRemoved(String key);
    }

}
