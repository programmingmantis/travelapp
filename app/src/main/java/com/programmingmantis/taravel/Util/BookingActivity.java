package com.programmingmantis.taravel.Util;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;

import com.programmingmantis.taravel.GeneratedItineraryListActivity;
import com.programmingmantis.taravel.Objects.TravelObject;
import com.programmingmantis.taravel.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BookingActivity extends AppCompatActivity implements View.OnTouchListener {

    View content_booking;

    AutoCompleteTextView clickedACTV;

    AutoCompleteTextView num_of_pax, emotion,
            purpose, date_start, pickup_location,
            destination_name, num_of_days;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        content_booking = findViewById(R.id.content_booking);

        num_of_pax = (AutoCompleteTextView) content_booking.findViewById(R.id.num_of_pax);
        emotion = (AutoCompleteTextView) content_booking.findViewById(R.id.emotion);
        purpose = (AutoCompleteTextView) content_booking.findViewById(R.id.purpose);
        date_start = (AutoCompleteTextView) content_booking.findViewById(R.id.date_start);
        pickup_location = (AutoCompleteTextView) content_booking.findViewById(R.id.pickup_location);
        destination_name = (AutoCompleteTextView) content_booking.findViewById(R.id.destination_name);
        num_of_days = (AutoCompleteTextView) content_booking.findViewById(R.id.num_of_days);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                //check declared fields before proceeding
                boolean fieldsOK = checker(
                        new AutoCompleteTextView[]{
                                num_of_days,
                                //destination_name,
                                pickup_location,
                                date_start,
                                purpose,
                                emotion,
                                num_of_pax});
                if (fieldsOK == true) {

                    List<TravelObject> travelObjectList = new ArrayList<>();

                    TravelDataUtil util = new TravelDataUtil();
                    travelObjectList.addAll(util.generateItinerary(emotion.getText().toString(),
                            purpose.getText().toString(), date_start.getText().toString(), destination_name.getText().toString(),
                            pickup_location.getText().toString(), num_of_pax.getText().toString()));
                    Intent intent = new Intent(BookingActivity.this, GeneratedItineraryListActivity.class);
                    Bundle bundle = new Bundle();
                    Log.d("test", purpose.getText().toString() + " " + emotion.getText().toString() + " " + travelObjectList.size());
                    bundle.putSerializable("travel_object_list", (Serializable) travelObjectList);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                }
            }
        });

        num_of_pax.setOnTouchListener(this);
        emotion.setOnTouchListener(this);
        purpose.setOnTouchListener(this);
        date_start.setOnTouchListener(this);
        pickup_location.setOnTouchListener(this);
        destination_name.setOnTouchListener(this);
        num_of_days.setOnTouchListener(this);


        emotion.setKeyListener(null);
        purpose.setKeyListener(null);
        date_start.setKeyListener(null);
        pickup_location.setKeyListener(null);
        destination_name.setKeyListener(null);


        //initialize arrays of actv
        autocomplete(emotion, getResources().getStringArray(R.array.emotion));
        autocomplete(purpose, getResources().getStringArray(R.array.purpose_solo));
        autocomplete(pickup_location, getResources().getStringArray(R.array.pickup_location));
        autocomplete(destination_name, getResources().getStringArray(R.array.destination_name));

        num_of_pax.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (num_of_pax.getText().toString().length() != 0) {
                    if (Integer.parseInt(num_of_pax.getText().toString()) == 1){
                        autocomplete(purpose, getResources().getStringArray(R.array.purpose_solo));
                    } else if (Integer.parseInt(num_of_pax.getText().toString()) == 2){
                        autocomplete(purpose, getResources().getStringArray(R.array.purpose_partner));
                    } else {
                        autocomplete(purpose, getResources().getStringArray(R.array.purpose_group));
                    }
                } else {
                    autocomplete(purpose, getResources().getStringArray(R.array.purpose_solo));
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        /*final int DRAWABLE_LEFT = 0;
        final int DRAWABLE_TOP = 1;
        final int DRAWABLE_RIGHT = 2;
        final int DRAWABLE_BOTTOM = 3;*/

        // TODO Auto-generated method stub
        switch (view.getId()) {
            case R.id.emotion:
                drawableRightClick(emotion, motionEvent, 2);//2 is for drawable right
                break;
            case R.id.purpose:
                drawableRightClick(purpose, motionEvent, 2);//2 is for drawable right
                break;
            case R.id.pickup_location:
                drawableRightClick(pickup_location, motionEvent, 2);//2 is for drawable right
                break;
            case R.id.destination_name:
                drawableRightClick(destination_name, motionEvent, 2);//2 is for drawable right
                break;

            case R.id.date_start:
                date_start.setError(null);
                if(motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if(motionEvent.getRawX() >= (date_start.getRight() - date_start.getCompoundDrawables()[2].getBounds().width())) {
                        AlertDialog.Builder date_builder = new AlertDialog.Builder(this);

                        date_builder.setCustomTitle(null);
                        final DatePicker dp_date = new DatePicker(this);
                        date_builder.setView(dp_date)
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        ArrayList<String> dateArrayList = new ArrayList<String>();
                                        int month = dp_date.getMonth() + 1;
                                        String s_month = "" + month;

                                        if (month <= 9) {
                                            s_month = "0" + month;
                                        }
                                        int day = dp_date.getDayOfMonth();
                                        int year = dp_date.getYear();
                                        String s_day = "" + day;

                                        dateArrayList.add(s_month);
                                        dateArrayList.add("" + day);
                                        dateArrayList.add("" + year);

                                        if(Double.parseDouble(String.valueOf(day)) < 10)
                                            s_day = "0" + day;

                                        //String date = year + month + day;
                                        String date = month + "/" + s_day + "/" + year;
                                        date_start.setText(date);
                                    }
                                })
                                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })

                                .show();
                    }
                }
                break;
            default:
                break;
        }

        return false;
    }


    void drawableRightClick (AutoCompleteTextView actv, MotionEvent motionEvent, int drawable){
        actv.setError(null);
        if(motionEvent.getAction() == MotionEvent.ACTION_UP) {
            if(motionEvent.getRawX() >= (actv.getRight() - actv.getCompoundDrawables()[drawable].getBounds().width())) {
                actv.setThreshold(0);
                actv.showDropDown();
            }
        }
    }

    //autocomplete array setter
    void autocomplete(final AutoCompleteTextView ac, String [] options){

        final ArrayAdapter<String> optionsAdapter;

        optionsAdapter = new ArrayAdapter<String>(BookingActivity.this,
                android.R.layout.simple_list_item_1,
                options);

        ac.setThreshold(0);
        ac.setAdapter(optionsAdapter);

        /*ac.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ac.showDropDown();
                return false;
            }
        });*/

    }

    boolean checker(AutoCompleteTextView[] actvfields){

        //prompt all edittext
        if (actvfields != null) {
            for(int i=0; i<actvfields.length; i++){
                AutoCompleteTextView currentField=actvfields[i];
                if(currentField.getText().toString().length()<=0){
                    currentField.requestFocus();
                    currentField.setError("Fill up this field");
                    currentField.requestFocus();
                } else {
                }
            }
        }

        return true;
    }

}
