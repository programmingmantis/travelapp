package com.programmingmantis.taravel.Util;

import com.programmingmantis.taravel.Objects.TravelObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aizer on 12/15/2016.
 */

public class TravelDataUtil {
    private List<TravelObject> travelList;

    public TravelDataUtil() {
        travelList = new ArrayList<TravelObject>();
        addData(travelList);
    }

    public static final String EMOTION_HAPPY = "happy";
    public static final String EMOTION_SAD = "sad";
    public static final String EMOTION_BORED = "bored";

    public static final String FOOD_HUNT = "food hunting";
    public static final String SOUL_SEARCH = "soul searching";
    public static final String BARKADA_TRIP = "barkada trip";
    public static final String OUTDOOR = "outdoor";
    public static final String SHOPPING = "shopping";
    public static final String LOOKING_FOR_INSPIRATION = "looking for inspiration";
    public static final String ROAD_TO_FOREVER = "road to forever";
    public static final String TEAM_BUILDING = "team building";
    public static final String FAMILY_BONDING = "family bonding";
    public static final String GETTING_TO_KNOW = "getting to know each other";

    public static final String BAGUIO = "Baguio";
    public static final String TAGAYTAY = "Tagaytay";
    public static final String ILOCOS = "Ilocos";
    public static final String SUBIC = "Subic";
    public static final String SAGADA = "Sagada";

    public List<TravelObject> getTravelList() {
        return travelList;
    }

    public void setTravelList(List<TravelObject> travelList) {
        this.travelList = travelList;
    }

    private void addData(List<TravelObject> travelList) {
        TravelObject travelObject = new TravelObject();
        travelObject.pickup_location = "Pasay";
        travelObject.destination_name = TAGAYTAY;
        travelObject.destination_desc = "Go To SkyRanch, Bag of Beans, Picnic Grove and Zipline";
        travelObject.emotion = EMOTION_HAPPY;
        travelObject.purpose = FOOD_HUNT + " " + BARKADA_TRIP + " " + SHOPPING;
        travelObject.package_price = "2000";
        travelObject.food_desc = "1Lunch and 1Dinner at Mahogany Market";

        travelList.add(travelObject);

        TravelObject travelObject1 = new TravelObject();
        travelObject1.pickup_location = "Cubao";
        travelObject1.destination_name = BAGUIO;
        travelObject1.destination_desc = "Go To Bell Church, Strawberry farm, White House, Botanical Garden, The Mansion, Grotto, Diplomat Hotel, Bencab Museum, Baguio Museum, Tam-Awan Village";
        travelObject1.emotion = EMOTION_SAD + " " + EMOTION_HAPPY;
        travelObject1.purpose = GETTING_TO_KNOW + " " + SOUL_SEARCH + " " + ROAD_TO_FOREVER;
        travelObject1.package_price = "4000";
        travelObject1.food_desc = "3Breakfast, 3lunch, 2dinner";

        travelList.add(travelObject1);

        TravelObject travelObject2 = new TravelObject();
        travelObject2.pickup_location = "Makati";
        travelObject2.destination_name = ILOCOS;
        travelObject2.destination_desc = "Go to Plaza Salcedo, Lights and Fountain Show, Calle Crisologo";
        travelObject2.emotion = EMOTION_HAPPY + " " + EMOTION_BORED;
        travelObject2.purpose = FAMILY_BONDING + " " +FOOD_HUNT + " " +OUTDOOR + " " +SHOPPING;
        travelObject2.package_price = "2000";
        travelObject2.food_desc = "2Breakfast, 2lunch, 1dinner";

        travelList.add(travelObject2);

        TravelObject travelObject3 = new TravelObject();
        travelObject3.pickup_location = "Cubao";
        travelObject3.destination_name = SUBIC;
        travelObject3.destination_desc = "Zoobic Safari Tour, Ocean Adventure Tour, Treetop adventure, Jetcamp";
        travelObject3.emotion = EMOTION_BORED + " " + EMOTION_SAD;
        travelObject3.purpose = TEAM_BUILDING + " " + OUTDOOR + " " + FOOD_HUNT;
        travelObject3.package_price = "2000";
        travelObject3.food_desc = "2Breakfast, 2lunch, 1dinner";

        travelList.add(travelObject3);

        TravelObject travelObject4 = new TravelObject();
        travelObject4.pickup_location = "Cubao";
        travelObject4.destination_name = SAGADA;
        travelObject4.destination_desc = "Lumiang Burial Cave, Sumaguing Cave, Pottery, Lake Danum, Hanging Coffins at Echo Valley, Bokong Falls";
        travelObject4.emotion = EMOTION_SAD;
        travelObject4.purpose = SOUL_SEARCH + " " + LOOKING_FOR_INSPIRATION;
        travelObject4.package_price = "2500";
        travelObject4.emotion = "sad";
        travelObject4.purpose = "soul search, looking for inspiration";
        travelObject4.package_price = "3500";
        travelObject4.food_desc = "4Breakfast, 4lunch, 3dinner";

        travelList.add(travelObject4);

        TravelObject travelObject5 = new TravelObject();
        travelObject5.pickup_location = "Cubao";
        travelObject5.destination_name = BAGUIO;
        travelObject5.destination_desc = "Go To Bell Church, Strawberry farm, White House, Botanical Garden, The Mansion, Grotto, Diplomat Hotel";
        travelObject5.emotion = EMOTION_HAPPY + " " + EMOTION_BORED;
        travelObject5.purpose = FAMILY_BONDING + " " +FOOD_HUNT + " " +OUTDOOR + " " +SHOPPING;
        travelObject5.package_price = "3200";
        travelObject5.food_desc = "2Breakfast, 2lunch, 1dinner";

        travelList.add(travelObject5);

        TravelObject travelObject6 = new TravelObject();
        travelObject6.pickup_location = "Cubao";
        travelObject6.destination_name = BAGUIO;
        travelObject6.destination_desc = "Go To Bell Church, Strawberry farm, White House";
        travelObject6.emotion = EMOTION_BORED + " " + EMOTION_SAD;
        travelObject6.purpose = TEAM_BUILDING + " " + OUTDOOR + " " + FOOD_HUNT;
        travelObject6.package_price = "2500";
        travelObject6.food_desc = "1Breakfast, 1lunch, 1 dinner";

        travelList.add(travelObject6);

        TravelObject travelObject7 = new TravelObject();
        travelObject7.pickup_location = "Makati";
        travelObject7.destination_name = ILOCOS;
        travelObject7.destination_desc = "Go to Plaza Salcedo, Lights and Fountain Show, Calle Crisologo, Pagudpud, Kapurpurawan, Paoay Church";
        travelObject7.emotion = EMOTION_HAPPY;
        travelObject7.purpose = FOOD_HUNT + " " + BARKADA_TRIP + " " + SHOPPING;
        travelObject7.package_price = "2400";
        travelObject7.food_desc = "3Breakfast, 3lunch, 2dinner";

        travelList.add(travelObject7);

        TravelObject travelObject8 = new TravelObject();
        travelObject8.pickup_location = "Cubao";
        travelObject8.destination_name = SAGADA;
        travelObject8.destination_desc = "Lumiang Burial Cave, Sumaguing Cave, Pottery, Lake Danum";
        travelObject8.emotion = EMOTION_HAPPY + " " + EMOTION_BORED;
        travelObject8.purpose = FAMILY_BONDING + " " +FOOD_HUNT + " " +OUTDOOR + " " +SHOPPING;
        travelObject8.package_price = "3000";
        travelObject8.food_desc = "3Breakfast, 3lunch, 2dinner";

        travelList.add(travelObject8);

    }

    public List<TravelObject> generateItinerary(String emotion, String purpose, String dateStart, String destinationName, String pickUpLocation, String pax){
        List<TravelObject> genList = new ArrayList<>();

        for(TravelObject travelObject : travelList){
            if(!destinationName.isEmpty() && travelObject.emotion.contains(emotion.toLowerCase()) && travelObject.purpose.contains(purpose.toLowerCase()) && travelObject.destination_name.contains(destinationName)) {
                travelObject.date_start = dateStart;
                travelObject.pickup_location = pickUpLocation;
                travelObject.num_of_pax = pax;
                travelObject.total_price = String.valueOf(Integer.valueOf(pax) * Integer.valueOf(travelObject.package_price));
                genList.add(travelObject);
            }
            else if(destinationName.isEmpty() && travelObject.emotion.contains(emotion.toLowerCase()) && travelObject.purpose.contains(purpose.toLowerCase())){
                travelObject.date_start = dateStart;
                travelObject.pickup_location = pickUpLocation;
                travelObject.num_of_pax = pax;
                travelObject.total_price = String.valueOf(Integer.valueOf(pax) * Integer.valueOf(travelObject.package_price));
                genList.add(travelObject);
            }
        }

        return genList;
    }
}
